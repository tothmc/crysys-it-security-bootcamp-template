#!/bin/bash
CHALLENGE_NAME=$1
if [ -d "./$CHALLENGE_NAME" ];then
    echo "'$CHALLENGE_NAME' directory exists"
    exit
else
    mkdir $CHALLENGE_NAME
    mkdir $CHALLENGE_NAME/solution
    touch $CHALLENGE_NAME/solution/solution.md
    echo "## $CHALLENGE_NAME - solution
    " >> $CHALLENGE_NAME/solution/solution.md
    mkdir $CHALLENGE_NAME/challenge_files
    touch $CHALLENGE_NAME/challenge_files/task.png
    touch $CHALLENGE_NAME/challenge_files/task.md
    echo '## '$CHALLENGE_NAME' - task  

### Task    
    
![](task.png)  
    
```
Desc:
```' >> $CHALLENGE_NAME/challenge_files/task.md
    echo "[-] - Press 'q' for quitting or press 'return' when you'have finished saving/creating task files in <challenge_name>/challenge_files subfolder"
    read -s -n 1 key
    while [[ $key != "" || $key != "q" ]]
    do
        if [[ $key = "" ]]; then 
            echo "[-] - Copying files from '$CHALLENGE_NAME/challenge_files/' to '$CHALLENGE_NAME/solution'"
            rsync -a --exclude={'task.md','task.png'} $CHALLENGE_NAME/challenge_files/ $CHALLENGE_NAME/solution
            echo "[+] - Successfully copied files from '$CHALLENGE_NAME/challenge_files/' to '$CHALLENGE_NAME/solution'"
            exit 0
        elif [[ $key = "q" ]]; then
            exit 0
        else
            echo "[!] - Press 'q' for quitting or press 'return' when you'have finished saving/creating task files in <challenge_name>/challenge_files subfolder. You pressed '$key'"
        fi
        read -s -n 1 key
    done
    cd $CHALLENGE_NAME
fi

