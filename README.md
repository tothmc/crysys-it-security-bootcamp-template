# CrySys IT security bootcamp - Training Project / Thematic project - 2022

## Linkek

- Website: [Link](https://www.crysys.hu/education/VIHIAL00)
- TAD: [Link](https://portal.vik.bme.hu/kepzes/targyak/VIHIAL00/)
- [**Bootcamp CTF challenge submission website**](https://bootcamp.crysys.hu/)

## Repo tree

```bash
├── challenge_template.sh
├── labor_01_intro
├── labor_02_crypto
│   ├── challenge_name_01
│   │   ├── solution
│   │   │   └── solution.md
│   │   └── challenge_files
│   │       ├── task.md
│   │       └── task.png
│   └── challenge_name_02
│       ├── solution
│       │   └── solution.md
│       └── challenge_files
│           ├── task.md
│           └── task.png
└── README.md


```

## TODO - at the beginning at each Training Projects lesson

```bash
$ mkdir labor_<numberOfLesson>_<subject>
$ cp challenge_template.sh labor_<numberOfLesson>_<subject>/challenge.sh
$ cd labor_<numberOfLesson>_<subject>
[labor_<numberOfLesson>_<subject>]$ ./challenge.sh <first_challenge_name>
[-] - Press 'q' for quitting or press'return' when you have finished saving/creating task files in <challenge_name>/challenge_files subfolder
[-] - Copying files from  '<first_challenge_name>/challenge_files/' to '<first_challenge_name>/solution'
[+] - Successfully copied files from '<first_challenge_name>/challenge_files/' to  '<first_challenge_name>/solution'
$ cd <first_challenge_name>

...

[labor_<numberOfLesson>_<subject>]$ ./challenge.sh <second_challenge_name>
[-] - Press 'q' for quitting or press 'return' when you have finished saving/creating task files in <challenge_name>/challenge_files subfolder
[-] - Copying files from '<second_challenge_name>/task_files/' to '<second_challenge_name>/solution'
[+] - Successfully copied files from '<second_challenge_name>/task_files/' to '<second_challenge_name>/solution'
$ cd <second_challenge_name>

```
